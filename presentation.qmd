---
title: "Python packaging"
subtitle: "with git, Gitlab-CI, and strict rules"
author: "Jean-Benoist Leger"
format:
  revealjs:
    output-file: slides
    smaller: true
    fig-format: svg
    code-fold: false
    highlight-style: github
    code-copy: true
    html-math-method: katex
    number-sections: true
    code-line-numbers: false
  html:
    output-file: cheatsheet
    toc: true
    fig-format: svg
    code-fold: false
    highlight-style: github
    code-copy: true
    html-math-method: katex
    number-sections: true
    code-line-numbers: false
---

## Outline {.unnumbered}

 - [Generalities]
 - [-@sec-git] [Create a git repository]
 - [-@sec-package] [Create the package]
 - [-@sec-doc] [Generate documentation]
 - [-@sec-tests] [Tests with `pytest`]
 - [Closing: Push to PyPI]

## Generalities {.unnumbered}

This document is licensed under 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license. _i.e._ you are
free to share, reuse, modify, or adapt this document, but you if you remove the
author name, the Russel Teapot will make you suffer for eternity.

**Convention used:**

 - 👮: required by mandatory rules (as [PEP8]).

   **→ No exception to the rule.**

 - 👪: common behavior from the community (as [google style]).

   **→ Exceptions allowed with a serious justification.**

 - 🧑‍🌾: recommended by the speaker.

   **→ Do what you want.**

[PEP8]: https://peps.python.org/pep-0008/
[google style]: https://google.github.io/styleguide/pyguide.html

## Create a git repository {#sec-git}

### First steps

 - Initialize the repo
 - Add an empty `.gitignore`
 - Associate with repo on gitlab (with `git remote add origin`)
 - Push

Advice 🧑‍🌾: use a shell with git functionalities.

Keep in mind the following rule 👪:

 - at **any** time: no file should be untracked.

   → each file must be either tracked or ignored.

---

### Pre-commit hooks

Apply strict rules when committing with pre-commit hooks 👪.

 - Install pre-commit (with pip, again).
 - 👪 Create the file `.pre-commit-config.yaml` with:
   ```yaml
   repos:
     - repo: https://github.com/pre-commit/pre-commit-hooks
       rev: v4.2.0
       hooks:
       -   id: trailing-whitespace
       -   id: end-of-file-fixer
     - repo: https://github.com/psf/black
       rev: 22.3.0
       hooks:
         - id: black
   ```

---

 - 🧑‍🌾 Add pylint to the pre-commit config by adding in `.pre-commit-config.yaml`:

   ```yaml
     - repo: local
       hooks:
         - id: pylint
           name: pylint
           entry: pylint
           language: system
           types: [python]
           args:
             [
               "-rn",
               "-sn",
               "--load-plugins=pylint.extensions.docparams",
               "--accept-no-yields-doc=no",
               "--accept-no-return-doc=no",
               "--accept-no-raise-doc=no",
               "--accept-no-param-doc=no",
             ]
   ```

 - Run `pre-commit init`.
 - Commit `.pre-commit-config.yaml` to your git repo.
 - Add a section _Contributing_ in `README.md` explaining `pre-commit` for
   contributors.

---

### Configure the CI

In the file `.gitlab-ci.yml`

 - Start by adding a default image and stages:
   ```yaml
   image: "python:3.8"

   stages:
     - linting
   ```
 - 👪 Add a job to check the formatting (here with [black]):
   ```yaml
   black:
     stage: linting
     image: registry.gitlab.com/pipeline-components/black:latest
     script:
       - black --check --verbose -- .
     tags:
       - docker
   ```

---

 - 🧑‍🌾 Add a job to check the linting with [pylint]:
   ```yaml
   pylint:
     stage: linting
     before_script:
       - pip install pylint
     script:
       - find . -type f -name "*.py" |
         xargs pylint
             --disable=import-error
             --load-plugins=pylint.extensions.docparams
             --accept-no-yields-doc=no
             --accept-no-return-doc=no
             --accept-no-raise-doc=no
             --accept-no-param-doc=no
   ```

---

### Use and force the CI validation

 - Commit `.gitlab-ci.yml` and push on `main`.
 - Check the pipeline success status.
 - Enforce merge requests by disabling push on main
   (*Settings* → *Repository* → *Protected branches*, select *No one* in
   *Allowed to push* for main branch) 👪.
 - Enforce pipeline success before merging. (*Settings* → *Merge requests* →
   *Merge checks*, select *Pipelines must succeed*) 🧑‍🌾 (most of the big
   projects use this convention, either humanly or automatically).

## Create the package {#sec-package}

### Clean the existing code

#### Objective

 - The code must follow the [PEP8] 👮.
 - The code must be formatted 👪.
 - Any exposed object must be documented 👪.
 - All the public API must be thinked to be stable 👪.

---

#### Tools

 - Use a formatter (as [black]) 👪.
 - Use a linter (as [pylint]) 👪, with strict rules 🧑‍🌾.
 - Your brain 🧑‍🌾. All other tools have their limitations.

[black]: https://github.com/psf/black
[pylint]: https://github.com/PyCQA/pylint

---

#### Steps

 - Install [black] and [pylint] (`pip` is your friend).
 - Configure [pylint] in your favorite editor.
 - Enable doc checking in pylint using these options:

   ```bash
   --load-plugins=pylint.extensions.docparams
   --accept-no-yields-doc=no
   --accept-no-return-doc=no
   --accept-no-raise-doc=no
   --accept-no-param-doc=no
   ```

 - Use black a first time (or configure black in your editor).
 - Edit your code, no error (👪), warning (👪) or comment
   (🧑‍🌾) should remain. (see following).
 - Document your code (👪). (see following)
 - Use black again.

---

#### Code edition

 - Private objects (not exposed to the users) must start by a `_`.
 - Every object exposed to the users must be documented.
 - If pylint is wrong, ignore locally the message by adding _e.g._:
   ```python
   def myfun(my_input): # pylint: disable=missing-function-docstring
      pass
   ```

   Use this at the end of a line (to have the smallest scope). **Use
   _disable_ sparingly.**

---

#### Docstrings

3 major docstring formats following [PEP257] 👮:

 - Native reStructuredText for [Sphinx], used in pytorch
 - [Google Style][gds], used in JAX
 - [Numpydoc][numpydoc], used in numpy, scipy, matplotlib, scikit-learn, JAX…

Advice:

 - 👪 Avoid using native reStructuredText, this format is not very human-readable.
 - 🧑‍🌾 For scientific projects use [Numpydoc], for other projects choose
   between [Numpydoc] and [Google style].

[PEP257]: https://peps.python.org/pep-0257/
[Sphinx]: http://sphinx-doc.org/
[gds]: https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings
[numpydoc]: https://numpydoc.readthedocs.io/en/latest/

---

### Create the package
#### Basic structure

Now the git repo should look like:

```
the_git_repo/
├── .gitignore
├── .gitlab-ci.yml
├── .pre-commit-config.yaml
└── README.md
```

Create a subdirectory `YOUR_PACKAGE_NAME`, and put an empty `__init__.py`
inside. You should have:

```
the_git_repo/
├── .gitignore
├── .gitlab-ci.yml
├── .pre-commit-config.yaml
├── README.md
└── YOUR_PACKAGE_NAME/
    └── __init__.py
```

---

Place all your sub-modules in `YOUR_PACKAGE_NAME` directory:

 - all modules imported when the top level module is imported
   must be imported in `__init__.py` (you can use relative import).
 - all modules masked to the user must start with a `_`.
 - all functions/classes accessible in the top level module must be imported in
   `__init__.py` (with `from … import …`)
 - if you have scripts, put them in hidden
   submodules.

**👪: Do not use relative import in submodule, use absolute import with package
name.**

---

For example, you get:

```
the_git_repo/
├── .gitignore
├── .gitlab-ci.yml
├── .pre-commit-config.yaml
├── README.md
└── YOUR_PACKAGE_NAME/
    ├── __init__.py
    ├── _cli.py
    ├── _data.py
    ├── _functions.py
    └── advanced.py
```

with `__init__.py` containing:

```python
from ._data import Vector, Species, Tree, Fungus
from ._functions import split, cluster, drop, detect
```

- `advanced` is a submodule not imported by default
- `_cli` is an hidden, not imported, submodule.

---

#### Test the structure

Test your package, go to the main folder (your git repo).

```default
toto@passoir ~/the_git_repo $ ipython
Python 3.9.2 (default, Feb 28 2021, 17:03:44)
Type 'copyright', 'credits' or 'license' for more information
IPython 7.30.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: import YOUR_PACKAGE_NAME

In [2]: YOUR_PACKAGE_NAME.cluster
Out[2]: <function YOUR_PACKAGE_NAME._functions.cluster(data, values=None)>
```

---

#### Choose a license

 - 👮: Put a `LICENSE` file.
 - 👪: for non-viral license (MIT, BSD-2…) it is good pratice to add a header in
   every file with the license.

🚸 For employees or public servants, the IP is owned by employer.
Copyright line must reference the employer in first position, the name of the
employees is given as an honorary title.

---

#### Package description

Create a `pyproject.toml`:

```toml
[build-system]
requires = ["setuptools", "setuptools-scm"]
build-backend = "setuptools.build_meta"

[tool.setuptools_scm]
```

With this config, the version is handled using git tag.

Complete the project description in `pyproject.toml`, keep the `dynamic` line:

```toml
[project]
name = "YOUR_PACKAGE_NAME"
dynamic = ["version"]
description = "The description of your package"
readme = "README.md"
license = {text = "MIT License"}
requires-python = ">=3.7"
keywords = []
authors = [
  {name = "Samuel Shenton", email = "sam.shenton@flat-earth.example.com"},
]
maintainers = [{name = "Samuel Shenton", email = "sam.shenton@flat-earth.example.com"},]
classifiers = [
  "License :: OSI Approved :: MIT License",
  "Development Status :: 3 - Alpha",
  "Programming Language :: Python",
]
dependencies = ["numpy",]
```

---

If you have scripts, declare them in the `pyproject.toml`:

```toml
[project.scripts]
your_program = "YOUR_PACKAGE_NAME._cli:main"
```

Here:

- `your_program` is the name of the created script
- `YOUR_PACKAGE_NAME._cli` is the module loaded to run the function
- `main` is the function ran by the script


Declarey your project's urls in `pyproject.toml`

```toml
[project.urls]
homepage = "https://example.com/"
repository = "https://example.com/"
```

You can use gitlab's url for the repository, and the website if you
have one.

---

#### Test the packaging

Install the module `build` (with pip).

- Local install:

   ```bash
   pip install .
   ```

   The package should be installed, go to another directory, run `ipython` and try to
   import your package. If you have a script, you can test the script.

 - Local build:

   ```bash
   python3 -m build
   ```

   You obtain a directory `dist/` with built packages.

At this step, you have a functional packaging. Following commit definition,
you should now commit this whole work at once.

**Reminder:** no untracked files. **The built packages must not be committed**. Please update `.gitignore`.

---

### Build and publish by the CI
#### Configuration
In `.gitlab-ci.yml`:

 - Add stages `build` and `publish`

   ```yaml
   stages:
     - linting
     - build
     - publish
   ```

 - Add the job for building the package

   ```yaml
   build_package:
     stage: build
     before_script:
       - pip install build
     script:
       - rm -rf dist/
       - python -m build
     artifacts:
       untracked: true
       expire_in: 1 week
     tags:
       - docker
   ```

   The package is available for one week in job's artifacts.

---
 
 - Add the job for publishing the package (only on tags)

   ```yaml
   publish_package:
     stage: publish
     before_script:
       - pip install twine
     script:
       - TWINE_PASSWORD=${CI_JOB_TOKEN}
         TWINE_USERNAME=gitlab-ci-token
         python -m twine upload
           --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
     tags:
       - docker
     only:
       - tags
   ```

   The package is uploaded only on new tags.

 - Commit, push, merge

---

Test the build through the CI.

 - The `publish_package` job is ran only on tag, a tag is a version of the
   package. Then, add a tag with the version (👪 [Semantic Versioning][semver] with `major.minor.patch`
is recommended.):
   ```bash
   git switch main
   git pull
   git tag -m 'version 0.0.1' 0.0.1
   git push --tags
   ```

 - Check the pipeline (on gitlab in *CI/CD*).

 - Check that the package is uploaded (on gitlab in *Packages and registries* →
   *Package and registry*. Copy the extra-index-url).

 - Clean the extra-index-url, auth is not needed for a public repo (remove
   `__token__:<your_personal_token>@`).

 - Update the `README.md`, add a **Install/Upgrade** section with the line
   ```bash
   pip install --upgrade --extra-index-url https://…
   ```
   (commit, push, merge)

[semver]: https://semver.org/

---

#### Security note

All users who can push tags can trigger the pipeline to publish a new version,
even if developpers cannot push commits to protected branch by default.

To avoid that, in Gitlab, go to *Settings* → *Repository* → *Protected tags*,
and *protect a tag* with:

 - tag: `*` (and select *Create wilcard `*`*)
 - allowed: Maintainers
 - *Protect*

## Generate documentation {#sec-doc}

### Sphinx config

 - Install `sphinx`, `sphinx_rtd_theme` with pip.

 - `cd` the base repo, and create a directory `doc`, and go in this directory.

 - In the `doc` directory, run `sphinx-quickstart`, answer questions:
   - *Separate source and build directories* **→** *YES*
   - *Project name* **→** you should know.
   - *Author name* **→** you should know.
   - *Project release* **→** keep empty.
   - *Project language* **→** *en*.

 - remove the `make.bat`, this is not usefull (maybe useful on windows _pour le
   peu qu'il m'en chaille_)

---

 - Edit the `source/conf.py`:
   - add to the first line (sorry, but this is necessary)
     ```python
     # pylint: skip-file
     ```
   - complete the file with:
     ```python
     from importlib.metadata import version
     ```
   - change the copyright line to add your employer between the year and your
     name if needed.
   - add the following lines to retrieve the version
     ```python
     release = version(project)
     version = release
     ```
   - change the `extensions` by:
     ```python
     extensions = [
         "sphinx.ext.napoleon",
         "sphinx_rtd_theme",
     ]
     ```
   - change `html_theme` by:
     ```python
     html_theme = "sphinx_rtd_theme"
     ```

---

### Documentation of the module

 - Create a documentation file for the module in the `doc/source/` directory, for
   example `module.rst`:

   ```rst
   Module documentation
   ====================

   .. automodule:: YOUR_PACKAGE_NAME
      :members:
      :imported-members:
      :inherited-members:
      :special-members: __init__
      :undoc-members:
      :show-inheritance:
   ```

 - Edit *toctree* of `doc/source/index.rst`, by setting the depth to 1, and adding
   the previous files you created.

   ```rst
   .. toctree::
      :maxdepth: 1
      :caption: Contents:

      ./module.rst
   ```

---

 - Run the build of the doc, `cd` to the base repo and run:
   ```bash
   make -C doc html
   ```

 - If there are errors, fix-them.

 - Once you get the directory `doc/build/html` open you browswer on it.

At this step, your doc works.

 - add the `doc/build/` directory in `.gitignore`
 - add `doc/Makefile`, `doc/source/conf.py` and `doc/source/*.rst` in git,
   commit…

---

### Complex packages

If your package is complex, having all the doc in one page can make it difficult to read. You can:

 - use `automodule` on submodule to generate doc only on a submodule using
   multiple rst file to separate modules.
 - use `autoclass` on class to generate doc only a class using multiple rst file
   to separate classes.
 - use hierarchical inclusion of rst to provide a tree in doc.

---

### Generate documentation for a CLI (with argparse)

 - Install `sphinx-argparse` (with pip).
 - Add `"sphinxarg.ext"` in `extensions` in `doc/source/conf.py`.
 - If necessary, edit your module to separate the parser construction and the usage
   of the parser.
 - Create a file in `doc/source/`, for example `cli.rst`:
   ```rst
   Command line program
   ====================

   .. argparse::
      :ref: YOUR_PACKAGE_NAME._cli._parser
      :prog: your_program
      :nodefault:
   ```
   where:
     - `your_program` is the name of the script
     - `YOUR_PACKAGE_NAME._cli` is the module loaded to run the function
     - `_parser` the function that builds the parser

---

 - Edit `doc/source/index.rst` and add `./cli.rst`:
   ```rst
   .. toctree::
      :maxdepth: 1
      :caption: Contents:

      ./module.rst
      ./cli.rst
   ```

 - Regenerate the doc.
 - Check, add files to git, commit…

---

### Generate doc in the CI and push to pages

Add the following jobs to the `.gitlab-ci.yml`:

```yml
build_doc:
  stage: build
  before_script:
    - pip install sphinx sphinx_rtd_theme
  script:
    - pip install .
    - make -C doc html
  artifacts:
    untracked: true
    expire_in: 1 week
  tags:
    - docker

pages:
  stage: publish
  script:
    - rm -rf public/
    - cp -r doc/build/html public/
  artifacts:
    paths:
    - public
  tags:
    - docker
  only:
    - tags
```

Notes: if you use `sphinx-argparse` add it to the `pip install` in the `before_script` section.
Also 🚱, the name `pages` of the job can not be changed if you want the pages to be properly uploaded.

---

Try it.

 - All good? Then commit, push, merge.
 - This job is only triggered on tags, so you should know what to do.
 - In Gitlab, go to *Settings* → *Pages* to retrieve the pages URL. With this
   URL, you can check the doc.
 - Update the url in the `pyproject.toml`
 - Update the `README.md` and add a link to the doc.

---

## Tests with `pytest` {#sec-tests}

### Generalities on tests

 - Tests are a very useful tool 👪
 - TDD is a good commercial argument (mostly bullshit for scientific usage) 🧑‍🌾
 - For scientific packages, writing fully exhaustive tests is just impossible 👪

Good practices 👪 are:

 - Write some tests for most common usages
 - For **every** bug, write tests to ensure this bug and closed friends
   will not come back in the future.

---

### Write the tests

Use [pytest].

 - In the base repo, create a directory `tests/`.
 - In this directory create files with names like `test_SOMETHING.py`
 - In file `test_FOO.py` write functions like `test_BAR` (you can also write
   other function and helpers not starting with `test_` but these functions are
   not executed until called by a function starting with `test_`).
 - Perform tests with `assert EXPECTED_TRUE_EXPR`.

Run the tests:

 - Install `pytest` (with pip).
 - Run `pytest`.

[pytest]: https://docs.pytest.org/

---

### Run the tests in the CI

 - Add a stage `tests` between `linting` and `build`:

   ```yaml
   stages:
     - linting
     - tests
     - build
     - publish
   ```

 - Add a job:

   ```yaml
   run_tests:
     stage: tests
     before_script:
       - pip install pytest
     script:
       - pip install .
       - pytest
   ```

 - Commit, push, merge.

## Closing: Push to PyPI {.unnumbered}

**🔞 PyPI is not a sandbox for childrens 🔞**

Use the gitlab registry as long your package is not completly functionnal and
ready for an external usage.

 - To create the package on PyPI, build the package locally (with 
   `python3 -m build`), and upload it to PyPI with twine.
 - Connect yourself to PyPI, and go to *Account* → *Manage* → *API token* and
   create a token for the project only.
 - On gitlab, in *Settings* → *CI/CD* → *Variables*, create a variable
   `PYPI_UPLOAD_TOKEN` with the value of the token. Select _protect_ and _mask_.
 - In the `publish_package` job in the CI, change the `twine` line by:
   ```yml
       - TWINE_PASSWORD=${PYPI_UPLOAD_TOKEN} TWINE_USERNAME=__token__ python -m twine upload dist/*
   ```
 - Test with a new tag.
 - Update the `README.md`
